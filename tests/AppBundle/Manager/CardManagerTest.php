<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Card;
use AppBundle\Entity\Center;
use AppBundle\Repository\CardRepository;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use const Grpc\CALL_ERROR_NOT_ON_SERVER;
use PHPUnit\Framework\TestCase;

class CardManagerTest extends TestCase
{
    private function getCardRepositoryMock()
    {
        $cardRepositoryMock = $this->createMock(CardRepository::class);
        $cardRepositoryMock
            ->method('findCardCountFromCenter')
            ->willReturn(125);
        return $cardRepositoryMock;
    }

    private function getEntityManagerMockWithRepository()
    {
        $cardRepositoryMock = $this->getCardRepositoryMock();
        $entityManagerMock  = $this->createMock('\Doctrine\ORM\EntityManager');
        $entityManagerMock->method('getRepository')
            ->willReturn($cardRepositoryMock);
        return $entityManagerMock;
    }

    private function getCenterMock()
    {
        $centerMock = $this->createMock(Center::class);
        return $centerMock;
    }

    private function getCenterMockWithGetId()
    {
        $centerMock = $this->getCenterMock();
        $centerMock->method('getId')->willReturn(12);
        return $centerMock;
    }

    private function getCardMock()
    {
        $cardMock = $this->createMock(Card::class);
        return $cardMock;
    }

    private function getCardMockWithMethod($theMethod, $theResult)
    {
        $cardMock = $this->getCardMock();
        $cardMock->method($theMethod)->willReturn($theResult);
        return $cardMock;
    }

    public function testGenerateCenterCode()
    {
        $entityManagerMock = $this->getEntityManagerMockWithRepository();

        $cardManager = new CardManager($entityManagerMock);
        $centerMock = $this->getCenterMockWithGetId();
        $cardMock = $this->getCardMockWithMethod('getCenter', $centerMock);

        $this->assertEquals('012', $cardManager->generateCenterCode($cardMock));
    }

    public function testGenerateCardCode()
    {
        $entityManagerMock = $this->getEntityManagerMockWithRepository();

        $cardManager = new CardManager($entityManagerMock);
        $centerMock = $this->getCenterMock();
        $cardMock = $this->getCardMockWithMethod('getCenter', $centerMock);

        $this->assertEquals('000126', $cardManager->generateCardCode($cardMock));
    }

    public function testGenerateChecksum()
    {
        $entityManagerMock = $this->getEntityManagerMockWithRepository();

        $cardManager = new CardManager($entityManagerMock);

        $centerMock = $this->getCenterMockWithGetId();
        $cardMock = $this->getCardMockWithMethod('getCenter', $centerMock);

        $this->assertEquals(3, $cardManager->generateChecksum($cardMock));
    }

    public function testGenerateCardPattern()
    {
        $entityManagerMock = $this->getEntityManagerMockWithRepository();

        $cardManager = new CardManager($entityManagerMock);

        $centerMock = $this->getCenterMockWithGetId();
        $cardMock = $this->getCardMockWithMethod('getCenter', $centerMock);

        $this->assertEquals('012-000126-3', $cardManager->generateCardPattern($cardMock));
    }
}
