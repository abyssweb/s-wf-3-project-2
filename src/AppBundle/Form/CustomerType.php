<?php

namespace AppBundle\Form;

use AppBundle\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname', TextType::class)
            ->add('firstname', TextType::class)
            ->add('nickname', TextType::class)
            ->add('address', TextType::class)
            ->add('zip_code', TextType::class)
            ->add('city', TextType::class)
            ->add('phone', TextType::class)
            ->add('birthdate', BirthdayType::class, [
                'format' => 'dd MM yyyy',
                'years' => range(1950, date("Y")),
            ])
            ->add('submit', SubmitType::class, [
                'label' => $options['submitLabel'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Customer::class,
            'submitLabel' => 'Register'
        ));
    }

    public function getBlockPrefix(): string
    {
        return 'app_bundle_customer_type';
    }
}
