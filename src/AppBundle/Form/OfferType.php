<?php

namespace AppBundle\Form;

use AppBundle\Entity\Offer;
use function date;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('shortDescription', TextType::class)
            ->add('description', TextType::class)
            ->add('promoCode', TextType::class)
            ->add('startAt', DateType::class, [
                'format' => 'dd MM yyyy',
                'years' => range(date('Y'), date('Y') + 5, 1),
            ])
            ->add('endAt', DateType::class, [
                'format' => 'dd MM yyyy',
                'years' => range(date('Y'), date('Y') + 5, 1),
            ])
            ->add('submit', SubmitType::class, [
                'label' => $options['submitLabel']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Offer::class,
            'submitLabel' => 'Submit',
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_offer_type';
    }
}
