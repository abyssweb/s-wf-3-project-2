<?php

namespace AppBundle\Form;

use AppBundle\Entity\GameData;
use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

class GameDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('score', IntegerType::class)
            ->add('victory', ChoiceType::class, [
                'choices' => ['Victorious' => 1, 'Defeated' => 0]
            ])
            ->add('submit', SubmitType::class, [
                'label' => $options['submitLabel'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => GameData::class,
            'submitLabel' => 'Submit',
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_game_data_type';
    }
}
