<?php

namespace AppBundle\Form;

use AppBundle\Entity\Card;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchCardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cardPattern', TextType::class)
            ->add('submit', SubmitType::class, [
                'label' => $options['submitLabel']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Card::class,
            'submitLabel' => 'Submit',
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_search_card_type';
    }
}
