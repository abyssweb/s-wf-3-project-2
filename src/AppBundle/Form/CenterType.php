<?php

namespace AppBundle\Form;

use AppBundle\Entity\Center;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CenterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('name', TextType::class)
                ->add('address', TextType::class)
                ->add('zipCode', TextType::class)
                ->add('city', TextType::class)
                ->add('submit', SubmitType::class, [
                    'label' => $options['submitLabel']
                ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Center::class,
            'submitLabel' => 'Valider'
        ));
    }

    public function getBlockPrefix(): string
    {
        return 'app_bundle_center_type';
    }
}
