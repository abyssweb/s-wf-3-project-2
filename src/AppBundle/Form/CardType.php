<?php

namespace AppBundle\Form;

use AppBundle\Entity\Card;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('center', ChoiceType::class, [
                'choices' => $options['centers'],
                'choice_label' => 'getName'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Add New Card'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Card::class,
            'centers' => [],
        ));
    }

    public function getBlockPrefix(): string
    {
        return 'app_bundle_card_type';
    }
}
