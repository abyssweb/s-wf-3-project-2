<?php

namespace AppBundle\Provider;

use AppBundle\Manager\CardManager;
use function array_rand;
use function array_sum;
use function mt_rand;

/**
 * Class GameDataCollector
 * Here is the Class simulating the Game Data Collection.
 * In Real Life, Datas would be collected from another system, and then integrated to the Loyalty System.
 * Here, it is simulated by this Provider.
 * In the Future, only the class would be updated to switch from simulation to production.
 * Right Now, the Simulator only create Teams based on real Loyalty Cards.
 */
class GameDataCollector
{

    private $cardManager;

    public function __construct(CardManager $cardManager)
    {
        $this->cardManager = $cardManager;
    }

    private function choosePlayerNumber(): int
    {
        return mt_rand(3, 10);
    }

    private function isPlayerNumberEven(int $playerNumber): bool
    {
        return ($playerNumber % 2 === 0) ? true : false;
    }

    private function getPlayers(int $playerNumber): array
    {
        $players = [];
        $activeCards = $this->cardManager->getRepository()->findBy([
            'disabledAt' => null,
        ]);
        for ($i = 0; $i < $playerNumber; $i++) {
            $key = array_rand($activeCards);
            $card = $activeCards[$key];
            $players[] = $card;
            unset($activeCards[$key]);
        }
        return $players;
    }

    private function getTeamSize(int $playerNumber): int
    {
        if ($this->isPlayerNumberEven($playerNumber) === false) {
            return 1;
        }
        return $playerNumber / 2;
    }

    private function getTeamNumber(int $playerNumber): int
    {
        if ($this->getTeamSize($playerNumber) === 1) {
            return $playerNumber;
        }
        return 2;
    }

    private function getScores(int $playerNumber): array
    {
        $scores = [];
        for ($i = 0; $i < $playerNumber; $i++) {
            $scores[] = mt_rand(0, 200000);
        }
        return $scores;
    }

    private function getTeams(array $players, array $scores, int $teamNumber, int $teamSize): array
    {
        $teams = [];

        for ($i = 0; $i < $teamNumber; $i++) {
            for ($j = 0; $j < $teamSize; $j++) {
                $key = array_rand($players);
                $teams[$i]['players'][$j] = $players[$key];
                $teams[$i]['scores'][$j] = $scores[$key];
                unset($players[$key]);
            }
            $teams[$i]['totalScore'] = array_sum($teams[$i]['scores']);
        }

        usort($teams, function ($a, $b) {
            if ($a['totalScore'] == $b['totalScore']) {
                return 0;
            }
            return ($a['totalScore'] > $b['totalScore']) ? -1 : 1;
        });

        return $teams;
    }

    public function getGameDatas(): array
    {
        $playerNumber = $this->choosePlayerNumber();
        $teamNumber = $this->getTeamNumber($playerNumber);
        $teamSize = $this->getTeamSize($playerNumber);
        $players = $this->getPlayers($playerNumber);
        $scores = $this->getScores($playerNumber);

        $gameDatas['teams'] = $this->getTeams(
            $players,
            $scores,
            $teamNumber,
            $teamSize
        );

        $i = 0;
        $j = 0;

        foreach ($gameDatas['teams'] as $team) {
            $k = 0;
            $victory = ($i === 0) ? true: false;
            foreach ($team['players'] as $player) {
                $gameDatas['details'][$j]['card'] = $player->getId();
                $gameDatas['details'][$j]['score'] = $team['scores'][$k];
                $gameDatas['details'][$j]['victory'] = $victory;
                $j++;
                $k++;
            }
            $i++;
        }
        return $gameDatas;
    }
}
