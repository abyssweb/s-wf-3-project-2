<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class PublicController extends Controller
{
    /**
     * @Route("/", name="public_homepage")
     */
    public function indexAction(): Response
    {
        return $this->render(':default:index.html.twig', []);
    }
}
