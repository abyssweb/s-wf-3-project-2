<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserTypeAdmin;
use AppBundle\Form\UserTypeAdminPassword;
use AppBundle\Form\UserTypeAdminRole;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/user")
 */
class AdminUserController extends Controller
{
    /**
     * @Route("/", name="admin_user_homepage")
     */
    public function indexAction(): Response
    {
        $userManager = $this->get('app.user_manager');
        $admins = $userManager->getRepository()->findBy(['role' => 'ROLE_ADMIN']);
        $staffs = $userManager->getRepository()->findBy(['role' => 'ROLE_STAFF']);

        return $this->render('admin/user/index.html.twig', [
            'admins' => $admins,
            'staffs' => $staffs
        ]);
    }

    /**
     * @Route("/new", name="admin_user_new")
     */
    public function newUserAction(Request $request): Response
    {
        $user = new User;
        $userManager = $this->get('app.user_manager');

        $form = $this->createForm(UserTypeAdmin::class, $user, [
            'submitLabel' => 'Add User'
            ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->save($user);
            $user = new User;

            $form = $this->createForm(UserTypeAdmin::class, $user, [
                'submitLabel' => 'Add User'
            ]);

            return $this->render('admin/user/new.html.twig', [
                'success' => true,
                'form' => $form->createView(),
            ]);
        }

        return $this->render('admin/user/new.html.twig', [
            'form' => $form->createView(),
            'success' => false
        ]);
    }

    /**
     * @Route("/update/role/{user}", name="admin_user_update_role")
     */
    public function updateUserRoleAction(Request $request, User $user): Response
    {
        $userManager = $this->get('app.user_manager');

        $form = $this->createForm(UserTypeAdminRole::class, $user, [
            'submitLabel' => 'Edit User'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->persistUser($user);

            return $this->render('admin/user/update.html.twig', [
                'form' => $form->createView(),
                'success' => true,
                'user' => $user,
            ]);
        }

        return $this->render('admin/user/update.html.twig', [
            'form' => $form->createView(),
            'success' => false,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/update/password/{user}", name="admin_user_update_password")
     */
    public function updateUserPasswordAction(Request $request, User $user): Response
    {
        $userManager = $this->get('app.user_manager');

        $form = $this->createForm(UserTypeAdminPassword::class, $user, [
            'submitLabel' => 'Edit User'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->persistUser($user);

            return $this->render('admin/user/update.html.twig', [
                'form' => $form->createView(),
                'success' => true,
                'user' => $user,
            ]);
        }

        return $this->render('admin/user/update.html.twig', [
            'form' => $form->createView(),
            'success' => false,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/delete/{user}", name="admin_user_delete")
     */
    public function deleteUserAction(User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();

        return $this->redirectToRoute('admin_user_homepage');
    }
}
