<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Center;
use AppBundle\Form\CenterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/center")
 */
class AdminCenterController extends Controller
{
    /**
     * @Route("/", name="admin_center_homepage")
     */
    public function indexAction(): Response
    {
        $centers = $this->get('app.center_manager')->getRepository()->findAll();

        return $this->render('admin/center/index.html.twig', [
            'centers' => $centers
        ]);
    }

    /**
     * @Route("/new", name="admin_center_new")
     */
    public function newCenterAction(Request $request): Response
    {
        $center = new Center();
        $centerManager = $this->get('app.center_manager');

        $form = $this->createForm(CenterType::class, $center, [
            'submitLabel' => 'Add Center'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $centerManager->save($center);
            $center = new Center;

            $form = $this->createForm(CenterType::class, $center, [
                'submitLabel' => 'Add Center'
            ]);

            return $this->render('admin/center/new.html.twig', [
                'form' => $form->createView(),
                'success' => true,
            ]);
        }

        return $this->render(':admin/center:new.html.twig', [
            'form' => $form->createView(),
            'success' => false,
        ]);
    }

    /**
     * @Route("/update/{center}", name="admin_center_update")
     */
    public function updateCenterAction(Request $request, Center $center): Response
    {
        $centerManager = $this->get('app.center_manager');

        $form = $this->createForm(CenterType::class, $center, ['submitLabel' => 'Edit Center']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $centerManager->save($center);

            return $this->render('admin/center/update.html.twig', [
                'form' => $form->createView(),
                'success' => true,
            ]);
        }

        return $this->render('admin/center/update.html.twig', [
            'form' => $form->createView(),
            'success' => false,
        ]);
    }

    /**
     * @Route("/close/{center}", name="admin_center_close")
     */
    public function closeCenterAction(Center $center): Response
    {
        $centerManager = $this->get('app.center_manager');
        $centerManager->closeCenter($center);

        return $this->redirectToRoute('admin_center_homepage');
    }

    /**
     * @Route("/show/{center}", name="admin_center_show")
     */
    public function showCenterAction(Center $center): Response
    {
        return $this->render('admin/center/show.html.twig', [
            'center' => $center,
        ]);
    }
}
