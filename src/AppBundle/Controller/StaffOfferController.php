<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Offer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use function strtoupper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/staff/offer")
 */
class StaffOfferController extends Controller
{
    /**
     * @Route("/show/all", name="staff_offer_show_all")
     */
    public function displayAllOfferAction(): Response
    {
        $offerRepository = $this->get('app.offer_manager')->getRepository();
        $offers = $offerRepository->findAll();

        return $this->render('staff/offer/all.html.twig', [
            'offers' => $offers,
        ]);
    }

    /**
     * @Route("/new", name="staff_offer_new")
     */
    public function newOfferAction(Request $request): Response
    {
        $offerManager = $this->get('app.offer_manager');
        $offer =  new Offer();
        $success = false;
        $error = false;

        $form = $this->createForm('AppBundle\Form\OfferType', $offer, [
            'submitLabel' => 'New Offer',
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $offerSearch = $offerManager->getRepository()->findOneBy([
                'promoCode' => strtoupper($offer->getPromoCode())
            ]);

            if ($offerSearch === null) {
                $offerManager->save($offer);
                $offer = new Offer();

                $form = $this->createForm('AppBundle\Form\OfferType', $offer, [
                    'submitLabel' => 'New Offer',
                ]);
                $success = true;
            } else {
                $error = true;
            }
        }

        return $this->render('staff/offer/new.html.twig', [
            'form' => $form->createView(),
            'success' => $success,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/edit/{offer}", name="staff_offer_edit")
     */
    public function editOfferAction(Request $request, Offer $offer): Response
    {
        $offerManager = $this->get('app.offer_manager');
        $success = false;
        $form = $this->createForm('AppBundle\Form\OfferType', $offer, [
            'submitLabel' => 'Edit Offer',
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $offerManager->save($offer);
            $success = true;
        }

        return $this->render('staff/offer/edit.html.twig', [
            'form' => $form->createView(),
            'success' => $success,
        ]);
    }

    /**
     * @Route("/delete/{offer}", name="staff_offer_delete")
     */
    public function deleteOfferAction(Offer $offer): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($offer);
        $entityManager->flush();

        return $this->redirectToRoute('staff_offer_homepage');
    }

    /**
     * @Route("/show/{offer}", name="staff_offer_show")
     */
    public function showOfferAction(Request $request, Offer $offer): Response
    {
        return $this->render('staff/offer/show.html.twig', [
            'offer' => $offer,
        ]);
    }
}
