<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/staff")
 */
class StaffController extends Controller
{
    /**
     * @Route("/", name="staff_homepage")
     */
    public function indexAction(Request $request): Response
    {
        $results = null;
        $error = false;

        $query = $request->request->get('search_query');

        if ($query !== null) {
            $results = $this->getSearchResults($query);
            if ($results !== null) {
                return $this->render('staff/results.html.twig', [
                    'results' => $results,
                    'error' => $error,
                    'query' => $query,
                ]);
            } else {
                $error = true;
            }
        }

        $cards = $this->getLastFiveCards();
        $users = $this->getLastFiveUsers();
        $offers = $this->getAvailableOffers();

        return $this->render('staff/index.html.twig', [
            'results' => $results,
            'error' => $error,
            'query' => $query,
            'cards' => $cards,
            'users' => $users,
            'offers' => $offers,
        ]);
    }

    private function getSearchResults(string $query): ?array
    {
        $searchManager = $this->get('app.search_manager');
        $results = $searchManager->search($query);
        return $results;
    }

    private function getLastFiveCards(): ?array
    {
        $cardRepository = $this->get('app.card_manager')->getRepository();
        $cards = $cardRepository->findLastFiveActiveCards();
        return $cards;
    }

    private function getLastFiveUsers(): ?array
    {
        $userRepository = $this->get('app.user_manager')->getRepository();
        $users = $userRepository->findLastFiveUsers();
        return $users;
    }

    private function getAvailableOffers(): array
    {
        $offerRepository = $this->get('app.offer_manager')->getRepository();
        $offers = $offerRepository->findAllAvailableOffers();
        return $offers;
    }
}
