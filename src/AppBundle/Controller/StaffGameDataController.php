<?php

namespace AppBundle\Controller;

use AppBundle\Entity\GameData;
use function file_get_contents;
use function file_put_contents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/staff/game_data")
 */
class StaffGameDataController extends Controller
{
    /**
     * @Route("/collect", name="staff_game_data_collect")
     */
    public function collectGameDatasAction(): Response
    {
        $gameDataCollector = $this->get('app.game_data_collector');
        $gameDatas = $gameDataCollector->getGameDatas();

        $serializer = $this->getSerializer();

        $jsonContent = $serializer->serialize($gameDatas['details'], 'json');
        $fileName = realpath($this->getParameter('kernel.project_dir')) . '/var/data/lastgame.json';
        file_put_contents($fileName, $jsonContent);

        return $this->render('staff/gamedata/show.html.twig', [
            'game' => $gameDatas,
        ]);
    }

    /**
     * @Route("/save", name="staff_game_data_save")
     */
    public function saveGameDatasAction(): Response
    {
        $fileName = realpath($this->getParameter('kernel.project_dir')) . '/var/data/lastgame.json';
        $jsonDatas = file_get_contents($fileName);

        $serializer = $this->getSerializer();
        $gameDatas = $serializer->decode($jsonDatas, 'json');

        $gameDataManager = $this->get('app.game_data_manager');
        $cardRepository = $this->get('app.card_manager')->getRepository();

        foreach ($gameDatas as $game) {
            $gameData = new GameData();
            $card = $cardRepository->findOneBy([
                'id' => $game['card'],
            ]);
            $gameData->setCard($card);
            $gameData->setScore($game['score']);
            $gameData->setVictory($game['victory']);
            $gameData->setDate(new \DateTime('now'));
            $gameDataManager->save($gameData);
        }
        return $this->render('staff/gamedata/index.html.twig', [
            'save' => true,
        ]);
    }

    private function getSerializer()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $serializer = new Serializer([$normalizer], [$encoder]);
        return $serializer;
    }
}
