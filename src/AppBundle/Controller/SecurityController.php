<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends Controller
{

    /**
     * @Route("/login", name="security_login")
     */
    public function loginAction(): Response
    {
        if ($this->getUser() !== null) {
            return $this->redirectToRoute('public_homepage');
        }
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastEmail = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_email' => $lastEmail,
            'error'         => $error
        ]);
    }

    /**
     * @Route("/register", name="security_register")
     */
    public function registerAction(Request $request): Response
    {
        if ($this->getUser() !== null) {
            return $this->redirectToRoute('public_homepage');
        }
        $userManager = $this->get('app.user_manager');
        $user = new User();
        $form = $this->createForm(UserType::class, $user, [
            'submitLabel' => 'Register',
        ]);
        $success = false;

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->saveUserForCustomer($user);
            $success = true;
        }

        return $this->render('security/register.html.twig', [
                'form' => $form->createView(),
                'success' => $success
            ]);
    }

    /**
     * @Route("/password_reset", name="security_password_reset")
     */
    public function passwordResetAction(): Response
    {
        return new Response();
    }
}
