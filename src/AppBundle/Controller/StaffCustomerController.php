<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Offer;
use AppBundle\Entity\User;
use AppBundle\Form\SearchOfferType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use function strtoupper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/staff/customer")
 */
class StaffCustomerController extends Controller
{
    /**
     * @Route("/show/{user}", name="staff_customer_show")
     */
    public function showCustomerAction(User $user): Response
    {
        $gameDataRepository = $this->get('app.game_data_manager')->getRepository();
        $games = $gameDataRepository->findAllGameDatasByUser($user);

        $offerRepository = $this->get('app.offer_manager')->getRepository();
        $offers =  $offerRepository->findAllAvailableOffersByUser($user);

        $cardRepository = $this->get('app.card_manager')->getRepository();
        $activeCard = $cardRepository->findOneBy(['user' => $user->getId(), 'disabledAt' => null]);

        return $this->render('staff/customer/show.html.twig', [
            'user' => $user,
            'games' => $games,
            'offers' => $offers,
            'activeCard' => $activeCard,
        ]);
    }

    /**
     * @Route("/search", name="staff_customer_search")
     */
    public function searchCustomer(Request $request): Response
    {
        $results = null;
        $error = false;

        $query = $request->request->get('search_query');

        if ($query !== null) {
            $searchManager = $this->get('app.search_manager');
            $results = $searchManager->search('user', $query);

            if ($results !== null) {
                return $this->render('staff/customer/results.html.twig', [
                    'results' => $results,
                    'error' => $error,
                    'query' => $query,
                ]);
            } else {
                $error = true;
            }
        }

        return $this->render('staff/customer/search.html.twig', [
            'results' => $results,
            'error' => $error,
            'query' => $query,
        ]);
    }

    /**
     * @Route("/link_card/{user}", name="staff_customer_link_card")
     */
    public function linkCardAction(Request $request, User $user): Response
    {
        $results = null;
        $error = false;

        $query = $request->request->get('search_query');

        if ($query !== null) {
            $results = $this->getSearchResults($query);
            if ($results !== null) {
                if ($results['type'] === 'card' || count($results['details']) === 1) {
                    $card = $results['details'][0];
                    if ($card->getUser() === null) {
                        $card->setUser($user);
                        $cardManager = $this->get('app.card_manager');
                        $cardManager->linkCardToUser($card);
                        return $this->redirectToRoute('staff_customer_show', [
                            'user' => $user->getId()
                        ]);
                    } else {
                        $error = true;
                    }
                } else {
                    $error = true;
                }
            } else {
                $error = true;
            }
        }

        return $this->render('staff/customer/add_card.html.twig', [
            'user' => $user,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/offer/give/{user}", name="staff_customer_give_offer")
     */
    public function giveOfferAction(Request $request, User $user): Response
    {
        $error = false;
        $offer = new Offer();

        $form = $this->createForm(SearchOfferType::class, $offer, [
            'submitLabel' => 'Add This Offer'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $offerManager = $this->get('app.offer_manager');
            $offer = $offerManager->getRepository()->findOneBy([
                'promoCode' => strtoupper($offer->getPromoCode())
            ]);
            if ($offer !== null) {
                $userOfferManager = $this->get('app.user_offer_manager');
                $userHasNotOffer = $userOfferManager->addOfferToUser($offer, $user);

                if ($userHasNotOffer === true) {
                    return $this->redirectToRoute('staff_customer_show', [
                        'user' => $user->getId()
                    ]);
                }
                $error = true;
            }
            $error = true;
        }

        return $this->render('staff/customer/give_offer.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'error' => $error,
        ]);
    }

    /**
     * @Route("/offer/use/{user}/{offer}", name="staff_customer_use_offer")
     */
    public function useOfferAction(Request $request, Offer $offer, User $user): Response
    {
        $userOfferManager = $this->get('app.user_offer_manager');
        $userOfferManager->useOfferFromUser($user, $offer);

        return $this->redirectToRoute('staff_customer_show', [
            'user' => $user->getId(),
        ]);
    }

    private function getSearchResults(string $query): ?array
    {
        $searchManager = $this->get('app.search_manager');
        $results = $searchManager->search($query);
        return $results;
    }
}
