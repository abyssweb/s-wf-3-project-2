<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Card;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Offer;
use AppBundle\Form\CustomerType;
use AppBundle\Form\SearchCardType;
use AppBundle\Form\SearchOfferType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends Controller
{
    /**
     * @Route("/account", name="customer_account")
     */
    public function accountAction(): Response
    {

        $user = $this->getUser();

        $cardRepository = $this->get('app.card_manager')->getRepository();
        $activeCard = $cardRepository->findOneBy(['user' => $user->getId(), 'disabledAt' => null]);

        $gameDataManager = $this->get('app.game_data_manager');
        $gameDataRepository = $gameDataManager->getRepository();
        $games = $gameDataRepository->findAllGameDatasByUser($user);

        $offerRepository = $this->get('app.offer_manager')->getRepository();
        $offers =  $offerRepository->findAllAvailableOffersByUser($this->getUser());

        $playerHistory = $gameDataManager->getPlayerHistory($user);

        return $this->render('user/account.html.twig', [
            'user' => $user,
            'card' => $activeCard,
            'player_history' => $playerHistory,
            'games' => $games,
            'offers' => $offers,
        ]);
    }

    /**
     * @Route("/account/add_card", name="customer_add_card")
     */
    public function addCardAction(Request $request): Response
    {
        $card = new Card();
        $user = $this->getUser();
        $error = false;

        $form = $this->createForm(SearchCardType::class, $card, [
            'submitLabel' => 'Add Card',
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cardManager = $this->get('app.card_manager');
            $card = $cardManager->getRepository()->findOneBy([
                'cardPattern' => $card->getCardPattern(),
                'user' => null
                ]);
            if ($card !== null) {
                $card->setUser($user);
                $cardManager->linkCardToUser($card);

                return $this->redirectToRoute('customer_account');
            }
            $error = true;
        }

        return $this->render('user/add_card.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/account/profile/new", name="customer_profile_new")
     */
    public function newProfileAction(Request $request): Response
    {
        $customer = new Customer();
        $success = false;
        $form = $this->createForm(CustomerType::class, $customer, [
            'submitLabel' => 'Save Profile',
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customerManager = $this->get('app.customer_manager');
            $customer->setUser($this->getUser());
            $customerManager->save($customer);
            return $this->redirectToRoute('customer_account');
        }

        return $this->render('user/new_profile.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/account/profile/edit", name="customer_profile_edit")
     */
    public function editProfileAction(Request $request): Response
    {
        $customer = $this->getUser()->getCustomer();

        $form = $this->createForm(CustomerType::class, $customer, [
            'submitLabel' => 'Save Profile'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customerManager = $this->get('app.customer_manager');
            $customerManager->save($customer);
            return $this->redirectToRoute('customer_account');
        }

        return $this->render('user/new_profile.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/account/promocode", name="customer_add_promo")
     */
    public function addPromoCodeAction(Request $request): Response
    {
        $user = $this->getUser();

        $error = false;
        $offer = new Offer();

        $form = $this->createForm(SearchOfferType::class, $offer, [
            'submitLabel' => 'Enter Promo Code'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $offerManager = $this->get('app.offer_manager');
            $offer = $offerManager->getRepository()->findOneBy([
                'promoCode' => strtoupper($offer->getPromoCode())
            ]);
            if ($offer !== null) {
                $userOfferManager = $this->get('app.user_offer_manager');
                $userHasNotOffer = $userOfferManager->addOfferToUser($offer, $user);

                if ($userHasNotOffer === true) {
                    return $this->redirectToRoute('customer_account');
                }
                $error = true;
            }
            $error = true;
        }

        return $this->render('user/add_promo.html.twig', [
            'form' => $form->createView(),
            'error' => $error,
        ]);
    }

    /**
     * @Route("/account/player_history", name="customer_player_history")
     */
    public function showPlayerHistoryAction(): Response
    {
        $gameDataRepository = $this->get('app.game_data_manager')->getRepository();
        $games = $gameDataRepository->findAllGameDatasByUser($this->getUser());

        return $this->render('user/game_history.html.twig', [
            'games' => $games,
        ]);
    }
}
