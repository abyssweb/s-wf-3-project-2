<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin_homepage")
     */
    public function indexAction(): Response
    {
        $userRepository = $this->get('app.user_manager')->getRepository();
        $admins = $userRepository->findBy(['role' => 'ROLE_ADMIN']);
        $staffs = $userRepository->findLastFiveStaffMembers();

        $centerRepository = $this->get('app.center_manager')->getRepository();
        $centers = $centerRepository->findLast5Centers();

        return $this->render('admin/index.html.twig', [
            'admins' => $admins,
            'staffs' => $staffs,
            'centers' => $centers,
        ]);
    }
}
