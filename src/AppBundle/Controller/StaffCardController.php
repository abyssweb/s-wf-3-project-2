<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Card;
use AppBundle\Form\CardType;
use AppBundle\Manager\CardManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/staff/card")
 */
class StaffCardController extends Controller
{
    /**
     * @Route("/new", name="staff_card_new")
     */
    public function newCardAction(Request $request): Response
    {
        $card = new Card();
        $cardManager = $this->get('app.card_manager');
        $centers = $this->get('app.center_manager')->getRepository()->findBy([ 'closedAt' => null ]);

        $form = $this->createForm(CardType::class, $card, ['centers' => $centers]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cardManager->generateNewCard($card);
            $cardPattern = $cardManager->generateCardPattern($card);
            $nfcId = $cardManager->generateFakeCardHexId();

            return $this->render('staff/card/new.nfc.html.twig', [
                'card' => $card,
                'nfc_id' => $nfcId
            ]);
        }

        return $this->render('staff/card/new.html.twig', [
            'form' => $form->createView(),
            'success' => false,
        ]);
    }

    /**
     * @Route("/print/{card}/{nfcId}", name="staff_card_print")
     */
    public function printCardAction(Request $request, Card $card, string $nfcId)
    {
        $cardManager = $this->get('app.card_manager');
        $card->setCardHexId($nfcId);
        $cardManager->save($card);

        return $this->redirectToRoute('staff_card_show', [
            'card' => $card->getId(),
        ]);
    }

    /**
     * @Route("/disable/{card}", name="staff_card_disable")
     */
    public function disableCardAction(CardManager $cardManager, Card $card): Response
    {
        $cardManager->disableCard($card);
        return $this->redirectToRoute('staff_card_show', [
            'card' => $card->getId()
        ]);
    }

    /**
     * @Route("/show/{card}", name="staff_card_show")
     */
    public function showCardAction(Card $card): Response
    {
        return $this->render('staff/card/show.html.twig', [
            'card' => $card
        ]);
    }
}
