<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadUserData extends Fixture
{
    public function load(ObjectManager $objectManager)
    {
        $userManager = $this->container->get('app.user_manager');

        // Fake Users without Customer
        $faker =  Factory::create('fr_FR');

        for ($i = 0; $i < 5; $i++) {
            $user = new User();
            $user->setEmail($faker->unique()->email);
            $user->setPlainPassword('IAmAUser');
            $user->setRole('ROLE_CUSTOMER');
            $userManager->save($user);
        }

        $user = new User();
        $user->setEmail('client@email.com');
        $user->setPlainPassword('IAmAClient');
        $user->setRole('ROLE_CUSTOMER');
        $userManager->save($user);

        // Admin User Creation
        $admin = new User();
        $admin->setEmail('admin@shinigami-laser.com');
        $admin->setPlainPassword('IAmTheAdmin');
        $admin->setRole('ROLE_ADMIN');
        $userManager->save($admin);

        // Staff Users Creation
        $staffs = [
            [
                'email' => 'barbara@shinigami-laser.com',
                'password' => 'IAmBarbara',
                'role' => 'ROLE_STAFF'
            ],
            [
                'email' => 'bob@shinigami-laser.com',
                'password' => 'BobNewbySuperHero',
                'role' => 'ROLE_STAFF'
            ],
            [
                'email' => 'joyce@shinigami-laser.com',
                'password' => 'WillWillWill',
                'role' => 'ROLE_STAFF'
            ]
        ];

        foreach ($staffs as $staff) {
            $user = new User();
            $user->setEmail($staff['email']);
            $user->setPlainPassword($staff['password']);
            $user->setRole($staff['role']);
            $userManager->save($user);
        }
    }
}
