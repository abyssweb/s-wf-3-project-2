<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Card;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadCardData extends Fixture
{

    public function load(ObjectManager $objectManager)
    {
        $faker =  Factory::create('fr_FR');
        $cardManager = $this->container->get('app.card_manager');
        $centerManager = $this->container->get('app.center_manager');
        $userManager = $this->container->get('app.user_manager');

        for ($i = 0; $i < 20; $i++) {
            $card = new Card();

            $center = $centerManager->getRepository()
                ->findOneBy(['id' => $faker->numberBetween(1, 10)]);
            $card->setCenter($center);

            $cardManager->save($card);
            $card->setCardCode($cardManager->generateCardCode($card));
            $card->setCreatedAt($faker->dateTimeBetween('- 1 year', 'now'));
            $card->setChecksum($cardManager->generateChecksum($card));
            $card->setCardPattern($cardManager->generateCardPattern($card));
            $card->setCardHexId($cardManager->generateFakeCardHexId());
            $cardManager->save($card);
        }

        for ($i = 0; $i < 15; $i++) {
            $card = $cardManager->getRepository()->findOneBy(['id' => $faker->unique()->numberBetween(1, 20)]);
            $user = $userManager->getRepository()->findOneBy(['id' => $faker->numberBetween(1, 16)]);

            $card->setUser($user);
            $cardManager->linkCardToUser($card);
            $user->addCard($card); // Had to do this. Fixtures and/or Doctrine are Stupid.
        }
    }

    public function getDependencies(): array
    {
        return [
            LoadCenterData::class,
            LoadCustomerData::class,
        ];
    }
}