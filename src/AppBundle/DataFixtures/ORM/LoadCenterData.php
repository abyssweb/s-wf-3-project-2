<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Center;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadCenterData extends Fixture
{

    public function load(ObjectManager $objectManager)
    {
        $centerManager = $this->container->get('app.center_manager');
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 10; $i++) {
            $center = new Center();

            $department = $this->getRandomDepartement();
            $zipCode = $this->generateValidZipCode($department);
            $city = $this->getValidCity($department);
            $name = 'Shinigami Laser ' . $city;

            $center->setName($name);
            $center->setAddress($faker->streetAddress);
            $center->setZipCode($zipCode);
            $center->setCity($city);

            $centerManager->save($center);
        }
    }

    private function getRandomDepartement(): string
    {
        $departements = ['21', '58', '71', '89'];
        $random = $this->getRandomIndexFromArray($departements);
        return $departements[$random];
    }

    private function generateValidZipCode($departement): string
    {
        $random = rand(10, 99) * 10;
        return $departement . $random;
    }

    private function getValidCity($departement): string
    {
        $villes = [
            '21' => ['Dijon', 'Beaune', 'Châteauneuf', 'Semur-en-Auxois'],
            '58' => ['Nevers', 'Clamecy', 'Decize', 'La Charité-sur-Loire'],
            '71' => ['Mâcon', 'Chalon-sur-Saône', 'Cluny', 'Autun', 'Beaujolais'],
            '89' => ['Auxerre', 'Vézelay', 'Chablis', 'Avallon', 'Sens', 'Noyers'],
        ];
        $ville = $villes[$departement][$this->getRandomIndexFromArray($villes[$departement])];

        return $ville;
    }

    private function getRandomIndexFromArray($array): int
    {
        return rand(0, count($array) - 1);
    }
}
