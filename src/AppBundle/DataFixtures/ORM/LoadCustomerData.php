<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Customer;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadCustomerData extends Fixture
{

    public function load(ObjectManager $objectManager)
    {

        $faker =  Factory::create('fr_FR');
        $userManager = $this->container->get('app.user_manager');
        $customerManager = $this->container->get('app.customer_manager');

        // Fixed User/Customer
        $virginieUser = new User();
        $virginieUser->setEmail('virginie@abyssweb.fr');
        $virginieUser->setPlainPassword('IAmVirginie');
        $virginieUser->setRole('ROLE_CUSTOMER');

        $userManager->save($virginieUser);

        $virginie = new Customer();
        $virginie->setLastname('Burlot');
        $virginie->setFirstname('Virginie');
        $virginie->setNickname('abyssweb');
        $virginie->setAddress('150, avenue du Général de Gaulle');
        $virginie->setZipCode('95000');
        $virginie->setCity('Chaumontel');
        $virginie->setPhone('0102030405');
        $virginie->setBirthdate(new \DateTime('1983-11-05 00:05:00'));
        $virginie->setUser($virginieUser);

        $customerManager->save($virginie);

        $virginieUser->setCustomer($virginie);
        $userManager->save($virginieUser);

        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setEmail($faker->unique()->email);
            $user->setPlainPassword('IAmACustomer');
            $user->setRole('ROLE_CUSTOMER');
            $userManager->save($user);

            $customer = new Customer();
            $customer->setLastname($faker->unique()->lastName);
            $customer->setFirstname($faker->unique()->firstName);
            $customer->setNickname($faker->unique()->userName);
            $customer->setAddress($faker->streetAddress);
            $customer->setZipCode($faker->postcode);
            $customer->setCity($faker->city);
            $customer->setPhone($faker->phoneNumber);
            $customer->setBirthdate(
                $faker->dateTimeInInterval(
                    $startDate = '-80 years',
                    $interval = '+ 1 days',
                    $timezone = date_default_timezone_get()
                )
            );
            $customer->setUser($user);
            $customerManager->save($customer);

            $user->setCustomer($customer);
            $userManager->save($user);
        }
    }
}
