<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\GameData;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use function mktime;

class LoadGameData extends Fixture
{

    public function load(ObjectManager $objectManager)
    {
        $faker =  Factory::create('fr_FR');
        $cardManager = $this->container->get('app.card_manager');
        $gameDataManager = $this->container->get('app.game_data_manager');

        for ($i = 0; $i < 60; $i++) {
            $gameData = new GameData();

            $card = $cardManager->getRepository()->findOneBy([
                'id' => $faker->numberBetween(1, 20)
            ]);

            $gameData->setCard($card);
            $gameData->setDate($faker->dateTimeBetween('- 1 year', 'now'));
            $gameData->setScore($faker->numberBetween(1000, 200000));
            if ($gameData->getScore() > 100000) {
                $gameData->setVictory(true);
            } else {
                $gameData->setVictory(false);
            }

            $gameDataManager->save($gameData);
        }
    }

    public function getDependencies(): array
    {
        return [
            LoadCardData::class,
        ];
    }
}
