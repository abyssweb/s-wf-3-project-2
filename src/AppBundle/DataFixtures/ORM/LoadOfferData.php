<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Offer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadOfferData extends Fixture
{
    public function load(ObjectManager $objectManager)
    {
        $offerManager = $this->container->get('app.offer_manager');

        $offers = [
            [
                'name' => '-20%',
                'shortDescription' => '-20% / 1 Journée',
                'description' => 'Moins 20% sur une journée complète !',
                'promoCode' => 'PROMO20',
                'startAt' => new \DateTime('2017-11-15 00:00:00'),
                'endAt' => new \DateTime('2018-01-31 00:00:00'),
            ],
            [
                'name' => 'Spécial Halloween',
                'shortDescription' => '2nde Partie Offerte',
                'description' => 'Venez pour Halloween, et la seconde partie est offerte !',
                'promoCode' => 'HALLO2017',
                'startAt' => new \DateTime('2017-10-23 00:00:00'),
                'endAt' => new \DateTime('2017-11-06 00:00:00'),
            ],
            [
                'name' => 'Spécial Noël',
                'shortDescription' => '1 partie offerte',
                'description' => 'Le Père Noël aime le Laser Game, 
                    et son préféré c\'est Shinigami Laser ! 
                    Une partie Offerte pendant les fêtes de Noël !',
                'promoCode' => 'NOEL2017',
                'startAt' => new \DateTime('2017-12-18 00:00:00'),
                'endAt' => new \DateTime('2017-12-31 00:00:00'),
            ],
            [
                'name' => 'Rentrée Laser',
                'shortDescription' => '-50% / 2nde partie',
                'description' => 'Une Rentrée en vainqueur grâce à l\'offre Rentrée de Shinigami Laser !
                    -50 % sur la seconde partie !',
                'promoCode' => 'RENTREE2017',
                'startAt' => new \DateTime('2017-08-28 00:00:00'),
                'endAt' => new \DateTime('2017-09-10 00:00:00'),
            ],
            [
                'name' => 'Happy New Laser Game',
                'shortDescription' => '1 partie Offerte',
                'description' => 'La bonne résolution de l\'année ? 
                    Devenir le meilleur au Laser Game ! 
                    Une partie Offerte pour toute visite dans l\'un de nos établissements !',
                'promoCode' => 'NEWYEAR2018',
                'startAt' => new \DateTime('2018-01-01 00:00:00'),
                'endAt' => new \DateTime('2018-01-07 00:00:00'),
            ],
        ];

        foreach ($offers as $offer) {
            $newOffer = new Offer();
            $newOffer->setName($offer['name']);
            $newOffer->setShortDescription($offer['shortDescription']);
            $newOffer->setDescription($offer['description']);
            $newOffer->setPromoCode($offer['promoCode']);
            $newOffer->setStartAt($offer['startAt']);
            $newOffer->setEndAt($offer['endAt']);
            $offerManager->save($newOffer);
        }
    }

    public function getDependencies(): array
    {
        return [
            LoadCustomerData::class,
        ];
    }
}
