<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Offer
 *
 * @ORM\Table(name="offer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OfferRepository")
 * @UniqueEntity(
 *     fields={"promoCode"},
 *     message="offer.promo_code_unique"
 * )
 */
class Offer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="text")
     */
    private $shortDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="promoCode", type="string", length=20)
     */
    private $promoCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startAt", type="datetime")
     */
    private $startAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endAt", type="datetime")
     */
    private $endAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserOffer", mappedBy="offer")
     */
    private $userOffers;

    /**
     * Get id
    */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     */
    public function setName(string $name): Offer
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set shortDescription
     */
    public function setShortDescription(string $shortDescription): Offer
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     */
    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    /**
     * Set description
     */
    public function setDescription(string $description): Offer
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set promoCode
     */
    public function setPromoCode(string $promoCode): Offer
    {
        $this->promoCode = $promoCode;

        return $this;
    }

    /**
     * Get promoCode
     */
    public function getPromoCode(): ?string
    {
        return $this->promoCode;
    }

    /**
     * Set startAt
     */
    public function setStartAt(\DateTime $startAt): Offer
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt
     */
    public function getStartAt(): ?\DateTime
    {
        return $this->startAt;
    }

    /**
     * Set endAt
     */
    public function setEndAt(\DateTime $endAt): Offer
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt
     */
    public function getEndAt(): ?\DateTime
    {
        return $this->endAt;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userOffers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add userOffer
     */
    public function addUserOffer(UserOffer $userOffer): Offer
    {
        $this->userOffers[] = $userOffer;

        return $this;
    }

    /**
     * Remove userOffer
     */
    public function removeUserOffer(UserOffer $userOffer)
    {
        $this->userOffers->removeElement($userOffer);
    }

    /**
     * Get userOffers
     */
    public function getUserOffers(): Collection
    {
        return $this->userOffers;
    }
}
