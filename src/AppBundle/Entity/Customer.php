<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Customer
 *
 * @ORM\Table(name="customer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomerRepository")
 * @UniqueEntity(
 *     fields={"nickname"},
 *     message="customer.nickname.unique"
 * )
 */
class Customer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="customer.lastname.not_blank"
     * )
     * @Assert\Length(
     *     min=2,
     *     max=255,
     *     minMessage="customer.lastname.min",
     *     maxMessage="customer.lastname.max"
     * )
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="customer.firstname.not_blank"
     * )
     * @Assert\Length(
     *     min=2,
     *     max=255,
     *     minMessage="customer.firstname.min",
     *     maxMessage="customer.firstname.max"
     * )
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    private $firstname;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="customer.nickname.not_blank"
     * )
     * @Assert\Length(
     *     min=2,
     *     max=255,
     *     minMessage="customer.nickname.min",
     *     maxMessage="customer.nickname.max"
     * )
     * @ORM\Column(name="nickname", type="string", length=255, unique=true)
     */
    private $nickname;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="customer.address.not_blank"
     * )
     * @Assert\Length(
     *     min=5,
     *     max=255,
     *     minMessage="customer.address.min",
     *     maxMessage="customer.address.max"
     * )
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="customer.zip.not_blank"
     * )
     * @Assert\Regex(
     *     pattern="/^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}$/",
     *     message="customer.zip.invalid"
     * )
     * @ORM\Column(name="zip_code", type="string", length=255)
     */
    private $zipCode;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="customer.city.not_blank"
     * )
     * @Assert\Length(
     *     min=2,
     *     max=255,
     *     minMessage="customer.city.min",
     *     maxMessage="customer.city.max"
     * )
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="customer.phone.not_blank"
     * )
     * @Assert\Length(
     *     min=10,
     *     max=20,
     *     minMessage="customer.phone.min",
     *     maxMessage="customer.phone.max"
     * )
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(
     *     message="customer.birthdate.not_blank"
     * )
     * @Assert\Date(
     *     message="customer.birthdate.invalid"
     * )
     * @ORM\Column(name="birthdate", type="datetime")
     */
    private $birthdate;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User", inversedBy="customer")
     */
    private $user;

    /**
     * Get id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set lastname
     */
    public function setLastname(string $lastname): Customer
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     */
    public function setFirstname(string $firstname): Customer
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * Set nickname
     */
    public function setNickname(string $nickname): Customer
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get nickname
     */
    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    /**
     * Set address
     */
    public function setAddress(string $address): Customer
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * Set zipCode
     */
    public function setZipCode(string $zipCode): Customer
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     */
    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    /**
     * Set city
     */
    public function setCity(string $city): Customer
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * Set phone
     */
    public function setPhone(string $phone): Customer
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * Set birthdate
     */
    public function setBirthdate(\DateTime $birthdate): Customer
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     */
    public function getBirthdate(): ?\DateTime
    {
        return $this->birthdate;
    }

    /**
     * Set user
     */
    public function setUser(User $user): Customer
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
