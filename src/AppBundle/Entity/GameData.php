<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GameData
 *
 * @ORM\Table(name="game_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameDataRepository")
 */
class GameData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="score", type="integer")
     */
    private $score;

    /**
     * @var bool
     *
     * @ORM\Column(name="victory", type="boolean")
     */
    private $victory;

    /**
     * @var Card
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Card", inversedBy="gameDatas")
     */
    private $card;

    /**
     * Get id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set date
     */
    public function setDate(\DateTime $date): GameData
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * Set score
     */
    public function setScore(int $score): GameData
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     */
    public function getScore(): ?int
    {
        return $this->score;
    }

    /**
     * Set victory
     */
    public function setVictory(bool $victory): GameData
    {
        $this->victory = $victory;

        return $this;
    }

    /**
     * Get victory
     */
    public function getVictory(): ?bool
    {
        return $this->victory;
    }

    /**
     * Set card
     */
    public function setCard(Card $card): GameData
    {
        $this->card = $card;

        return $this;
    }

    /**
     * Get card
     */
    public function getCard(): Card
    {
        return $this->card;
    }
}
