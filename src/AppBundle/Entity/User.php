<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity(
 *     fields={"email"},
 *     message="user.email.unique"
 * )
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="user.email.not_blank"
     * )
     * @Assert\Email(
     *     message="user.email.invalid"
     * )
     * @ORM\Column(name="email", type="string")
     */
    private $email;

    /**
     * @Assert\NotBlank(
     *     message="user.password.not_blank")
     * )
     * @Assert\Length(
     *     min=8,
         *     minMessage="user.password.min"
     * )
     */
    private $plainPassword;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=4096)
     */
    private $password;

    /**
     * @var string
     *
     * @Assert\Choice(
     *     choices={"ROLE_CUSTOMER", "ROLE_STAFF", "ROLE_ADMIN"},
     *     strict=true
     *     )
     * @ORM\Column(name="role", type="string", length=20)
     */
    private $role;

    /**
     * @var Customer
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Customer", mappedBy="user")
     */
    private $customer;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Card", mappedBy="user")
     */
    private $cards;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserOffer", mappedBy="user")
     */
    private $userOffers;

    /**
     * Get id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set email
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Get email as username
     */
    public function getUsername(): string
    {
        return $this->getEmail();
    }

    /**
     * Set password
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     *
     * Set plainPassword
     */
    public function setPlainPassword(string $password)
    {
        $this->plainPassword = $password;
    }

    /**
     * Get  plainPassword
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * Set role
     */
    public function setRole(string $role): User
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     */
    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * Set customer
     */
    public function setCustomer(Customer $customer): User
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     */
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cards = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userOffers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add card
     */
    public function addCard(Card $card): User
    {
        $this->cards[] = $card;

        return $this;
    }

    /**
     * Remove card
     */
    public function removeCard(Card $card)
    {
        $this->cards->removeElement($card);
    }

    /**
     * Get cards
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    /**
     * Get salt
     *
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Get Roles
     */
    public function getRoles(): array
    {
        return [$this->getRole()];
    }

    /*
     * Erase Credentials
     *
     * @return null
     */
    public function eraseCredentials()
    {
        return null;
    }

    /**
     * Add userOffer
     */
    public function addUserOffer(UserOffer $userOffer): User
    {
        $this->userOffers[] = $userOffer;

        return $this;
    }

    /**
     * Remove userOffer
     */
    public function removeUserOffer(UserOffer $userOffer)
    {
        $this->userOffers->removeElement($userOffer);
    }

    /**
     * Get userOffers
     */
    public function getUserOffers(): Collection
    {
        return $this->userOffers;
    }
}
