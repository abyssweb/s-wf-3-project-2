<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Card
 *
 * @ORM\Table(name="card")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CardRepository")
 */
class Card
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="checksum", type="integer", nullable=true)
     */
    private $checksum;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="linked_at", type="datetime", nullable=true)
     */
    private $linkedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="disabled_at", type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @var Center
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Center", inversedBy="cards")
     */
    private $center;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\GameData", mappedBy="card")
     */
    private $gameDatas;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="cards")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="pattern", type="string", nullable=true)
     */
    private $cardPattern;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", nullable=true)
     */
    private $cardCode;

    /**
     * @var string
     *
     * @ORM\Column(name="card_hex_id", type="string", nullable=true)
     */
    private $cardHexId;

    /**
     * Get id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set checksum
     */
    public function setChecksum(int $checksum): Card
    {
        $this->checksum = $checksum;

        return $this;
    }

    /**
     * Get checksum
     */
    public function getChecksum(): int
    {
        return $this->checksum;
    }

    /**
     * Set createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): Card
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->gameDatas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set linkedAt
     */
    public function setLinkedAt(\DateTime $linkedAt): Card
    {
        $this->linkedAt = $linkedAt;

        return $this;
    }

    /**
     * Get linkedAt
     */
    public function getLinkedAt(): ?\DateTime
    {
        return $this->linkedAt;
    }

    /**
     * Set disabledAt
     */
    public function setDisabledAt(\DateTime $disabledAt): Card
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    /**
     * Get disabledAt
     */
    public function getDisabledAt(): ?\DateTime
    {
        return $this->disabledAt;
    }

    /**
     * Set center
     */
    public function setCenter(Center $center = null): Card
    {
        $this->center = $center;

        return $this;
    }

    /**
     * Get center
     */
    public function getCenter(): ?Center
    {
        return $this->center;
    }

    /**
     * Add gameData
     */
    public function addGameData(GameData $gameData): Card
    {
        $this->gameDatas[] = $gameData;

        return $this;
    }

    /**
     * Remove gameData
     */
    public function removeGameData(GameData $gameData)
    {
        $this->gameDatas->removeElement($gameData);
    }

    /**
     * Get gameDatas
     */
    public function getGameDatas(): Collection
    {
        return $this->gameDatas;
    }

    /**
     * Set user
     */
    public function setUser(User $user = null): Card
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * Set cardPattern
     */
    public function setCardPattern(string $cardPattern): Card
    {
        $this->cardPattern = $cardPattern;

        return $this;
    }

    /**
     * Get cardPattern
     */
    public function getCardPattern(): ?string
    {
        return $this->cardPattern;
    }

    /**
     * Set cardCode
     */
    public function setCardCode(string $cardCode): Card
    {
        $this->cardCode = $cardCode;

        return $this;
    }

    /**
     * Get cardCode
     */
    public function getCardCode(): ?string
    {
        return $this->cardCode;
    }

    /**
     * Set cardHexId
     */
    public function setCardHexId(string $cardHexId): Card
    {
        $this->cardHexId = $cardHexId;

        return $this;
    }

    /**
     * Get cardHexId
     */
    public function getCardHexId(): ?string
    {
        return $this->cardHexId;
    }
}
