<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserOffer
 *
 * @ORM\Table(name="user_offer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserOfferRepository")
 */
class UserOffer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="used", type="boolean")
     */
    private $used;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="userOffers")
     */
    private $user;

    /**
     * @var Offer
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Offer", inversedBy="userOffers")
     */
    private $offer;

    /**
     * Get id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get used
     */
    public function getUsed(): bool
    {
        return $this->used;
    }

    /**
     * Set used
     */
    public function setUsed(bool $used)
    {
        $this->used = $used;
    }

    /**
     * Get User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * Set User
     */
    public function setUser(User $user): UserOffer
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get Offer
     */
    public function getOffer(): Offer
    {
        return $this->offer;
    }

    /**
     * Set Offer
     */
    public function setOffer(Offer $offer): UserOffer
    {
        $this->offer = $offer;
        return $this;
    }
}
