<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Center
 *
 * @ORM\Table(name="center")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CenterRepository")
 */
class Center
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="center.name.not_blank"
     * )
     * @Assert\Length(
     *     min=4,
     *     max=255,
     *     minMessage="center.name.min",
     *     maxMessage="center.name.max"
     * )
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="center.address.not_blank"
     * )
     * @Assert\Length(
     *     min=4,
     *     max=255,
     *     minMessage="center.address.min",
     *     maxMessage="center.address.max"
     * )
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="center.zip.not_blank"
     * )
     * @Assert\Regex(
     *     pattern="/^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}$/",
     *     message="center.zip.invalid"
     * )
     * @ORM\Column(name="zip_code", type="string", length=255)
     */
    private $zipCode;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="center.city.not_blank"
     * )
     * @Assert\Length(
     *     min=4,
     *     max=255,
     *     minMessage="center.city.min",
     *     maxMessage="center.city.max"
     * )
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="closed_at", type="datetime", nullable=true)
     */
    private $closedAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Card", mappedBy="center")
     */
    private $cards;

    /**
     * Get id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     */
    public function setName(string $name): Center
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set address
     */
    public function setAddress(string $address): Center
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * Set zipCode
     */
    public function setZipCode(string $zipCode): Center
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     */
    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    /**
     * Set city
     */
    public function setCity(string $city): Center
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     */
    public function getCity(): ?string
    {
        return $this->city;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cards = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add card
     */
    public function addCard(Card $card): Center
    {
        $this->cards[] = $card;

        return $this;
    }

    /**
     * Remove card
     */
    public function removeCard(Card $card)
    {
        $this->cards->removeElement($card);
    }

    /**
     * Get cards
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    /**
     * Set closedAt
     */
    public function setClosedAt(\DateTime $closedAt): Center
    {
        $this->closedAt = $closedAt;

        return $this;
    }

    /**
     * Get closedAt
     */
    public function getClosedAt(): ?\DateTime
    {
        return $this->closedAt;
    }
}
