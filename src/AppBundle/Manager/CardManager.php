<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Card;
use AppBundle\Repository\CardRepository;
use Doctrine\ORM\EntityManagerInterface;
use function dechex;
use function mt_rand;
use function str_split;
use function strlen;

class CardManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getRepository(): CardRepository
    {
        return $this->entityManager->getRepository(Card::class);
    }

    public function save(Card $card)
    {
        $this->entityManager->persist($card);
        $this->entityManager->flush();
    }

    public function generateNewCard(Card $card)
    {
        $card->setCardCode($this->generateCardCode($card));
        $card->setChecksum($this->generateChecksum($card));
        $card->setCreatedAt(new \DateTime('now'));
        $card->setCardPattern($this->generateCardPattern($card));
        $this->save($card);
    }

    public function linkCardToUser(Card $card)
    {
        $user = $card->getUser();
        $cards = $user->getCards()->toArray();
        if (!empty($cards)) {
            foreach ($cards as $item) {
                if ($item->getDisabledAt() === null) {
                    $this->disableCard($item);
                }
            }
        }
        $card->setLinkedAt(new \DateTime('now'));
        $this->save($card);
    }

    public function disableCard(Card $card)
    {
        $card->setDisabledAt(new \DateTime('now'));
        $this->save($card);
    }

    public function generateCenterCode(Card $card): string
    {
        $centerCode = (string) $card->getCenter()->getId();
        while (strlen($centerCode) < 3) {
            $centerCode = '0' . $centerCode;
        }
        return $centerCode;
    }

    public function generateCardCode(Card $card): string
    {
        $cardCode = $this->getCardCountFromCenter($card);
        $cardCode++;
        $cardCode = (string) $cardCode;

        while (strlen($cardCode) < 6) {
            $cardCode = '0' . $cardCode;
        }
        return $cardCode;
    }

    public function getCardCountFromCenter(Card $card): int
    {
        $count = $this
            ->getRepository()
            ->findCardCountFromCenter($card->getCenter());
        return $count;
    }

    public function generateChecksum(Card $card): int
    {
        $centerCode = str_split($this->generateCenterCode($card));
        $cardCode = str_split($this->generateCardCode($card));

        $centerCodeSum = 0;
        $cardCodeSum = 0;

        foreach ($centerCode as $item) {
            $centerCodeSum += $item;
        }
        foreach ($cardCode as $item) {
            $cardCodeSum += $item;
        }
        $checksum = ($centerCodeSum + $cardCodeSum) % 9;

        return $checksum;
    }

    public function generateCardPattern(Card $card): string
    {
        return $this->generateCenterCode($card)
            . '-'
            . $this->generateCardCode($card)
            . '-'
            . $this->generateChecksum($card);
    }

    public function generateFakeCardHexId(): string
    {
        $hexId = '';
        for ($i = 0; $i < 4; $i++) {
            $hexId .= dechex(mt_rand(0, 255));
            $hexId .= ($i !== 3) ? ':' : '';
        }
        return $hexId;
    }
}
