<?php

namespace AppBundle\Manager;

use AppBundle\Entity\GameData;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class GameDataManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getRepository()
    {
        return $this->entityManager->getRepository(GameData::class);
    }

    public function save(GameData $gameData)
    {
        $this->entityManager->persist($gameData);
        $this->entityManager->flush();
    }

    public function generateNewGameData(GameData $gameData)
    {
        $gameData->setDate(new \DateTime('now'));
        $this->save($gameData);
    }

    public function getPlayerHistory(User $user)
    {
        $playerHistory = [];

        $playerHistory['victoryCount'] = $this
            ->getRepository()
            ->findVictoryCount($user);
        $playerHistory['defeatCount'] = $this
            ->getRepository()
            ->findDefeatCount($user);
        $playerHistory['ratio'] = $this
            ->getVictoryRatio(
                $playerHistory['victoryCount'],
                $playerHistory['defeatCount']
            );
        $playerHistory['gameCount'] = $this
            ->getRepository()
            ->findGameCount($user);
        $playerHistory['scoreHigh'] = $this
            ->getRepository()
            ->findHighScore($user);
        $playerHistory['scoreLow'] = $this
            ->getRepository()
            ->findLowScore($user);
        $playerHistory['scoreAvg'] = (int) $this
            ->getRepository()
            ->findAvgScore($user);

        return $playerHistory;
    }

    public function getVictoryRatio($win, $lose): float
    {
        if ($lose !== '0' && $lose !== 0) {
            return round($win / $lose, 2);
        }
        return round($win, 2);
    }
}
