<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Offer;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use function strtoupper;

class OfferManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getRepository()
    {
        return $this->entityManager->getRepository(Offer::class);
    }

    public function save(Offer $offer)
    {
        $offer->setPromoCode(strtoupper($offer->getPromoCode()));
        $this->entityManager->persist($offer);
        $this->entityManager->flush();
    }
}
