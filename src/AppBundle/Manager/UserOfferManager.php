<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Offer;
use AppBundle\Entity\User;
use AppBundle\Entity\UserOffer;
use Doctrine\ORM\EntityManagerInterface;

class UserOfferManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getRepository()
    {
        return $this->entityManager->getRepository(UserOffer::class);
    }

    public function addOfferToUser(Offer $offer, User $user)
    {
        $userOffer = new UserOffer();
        $userHasOffer = $this->getRepository()->findBy([
            'user' => $user,
            'offer' => $offer
        ]);
        if ($userHasOffer === []) {
            $userOffer->setUser($user);
            $userOffer->setOffer($offer);
            $userOffer->setUsed(false);
            $this->save($userOffer);
            return true;
        }
        return false;
    }

    public function useOfferFromUser(User $user, Offer $offer)
    {
        $userOffer = $this->getRepository()->findOneBy([
            'user' => $user,
            'offer' => $offer
        ]);
        $userOffer->setUsed(true);
        $this->save($userOffer);
    }

    public function save(UserOffer $userOffer)
    {
        $this->entityManager->persist($userOffer);
        $this->entityManager->flush();
    }
}
