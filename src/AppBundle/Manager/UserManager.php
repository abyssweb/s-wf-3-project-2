<?php

namespace AppBundle\Manager;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class UserManager
{
    private $entityManager;
    private $encoderFactory;
    private $tokenStorage;
    private $router;

    public function __construct(
        EntityManagerInterface $entityManager,
        EncoderFactoryInterface $encoderFactory,
        TokenStorageInterface $tokenStorage,
        RouterInterface $router
    ) {
        $this->entityManager = $entityManager;
        $this->encoderFactory = $encoderFactory;
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }

    public function getRepository()
    {
        return $this->entityManager->getRepository(User::class);
    }

    public function generatePassword(User $user)
    {
        $hashedPassword = $this->encoderFactory
            ->getEncoder(User::class)
            ->encodePassword($user->getPlainPassword(), $user);
        $user->setPassword($hashedPassword);
    }

    public function save(User $user)
    {
        $this->generatePassword($user);
        $this->persistUser($user);
    }

    public function persistUser(User $user)
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function saveUserForCustomer(User $user)
    {
        $user->setRole('ROLE_CUSTOMER');
        $this->save($user);
    }
}
