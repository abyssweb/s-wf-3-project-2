<?php

namespace AppBundle\Manager;

class SearchManager
{

    private $userManager;
    private $cardManager;

    public function __construct(
        UserManager $userManager,
        CardManager $cardManager
    ) {
        $this->userManager = $userManager;
        $this->cardManager = $cardManager;
    }

    public function search($query): ?array
    {
        $result = $this->searchUser($query);
        if ($result !== null) {
            $results['type'] = 'user';
            $results['details'] = $result;
            return $results;
        }
        $result = $this->searchCard($query);
        if ($result !== null) {
            $results['type'] = 'card';
            $results['details'] = $result;
            return $results;
        }

        return null;
    }

    private function searchUser($query): ?array
    {
        $result = $this->searchUserByEmail($query);
        if (!empty($result)) {
            return $result;
        }

        $result = $this->searchUserByCustomerName($query);
        if (!empty($result)) {
            return $result;
        }

        return null;
    }

    private function searchCard($query): ?array
    {
        $result = $this->searchCardByPattern($query);
        if (!empty($result)) {
            return $result;
        }

        $result = $this->searchCardByHexId($query);
        if (!empty($result)) {
            return $result;
        }

        return null;
    }

    private function searchUserByEmail($query): ?array
    {
        return $this
            ->userManager
            ->getRepository()
            ->findUsersByEmailLike($query);
    }

    private function searchUserByCustomerName($query): ?array
    {
        return $this
            ->userManager
            ->getRepository()
            ->findUserByCustomerName($query);
    }

    private function searchCardByPattern($query): ?array
    {
        return $this
            ->cardManager
            ->getRepository()
            ->findCardByPattern($query);
    }

    private function searchCardByHexId($query): ?array
    {
        return $this
            ->cardManager
            ->getRepository()
            ->findCardByHexId($query);
    }
}
