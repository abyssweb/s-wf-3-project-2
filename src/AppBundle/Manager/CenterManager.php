<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Center;
use Doctrine\ORM\EntityManagerInterface;

class CenterManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getRepository()
    {
        return $this->entityManager->getRepository(Center::class);
    }

    public function save(Center $center)
    {
        $this->entityManager->persist($center);
        $this->entityManager->flush();
    }

    public function closeCenter(Center $center)
    {
        $center->setClosedAt(new \DateTime('now'));
        $this->save($center);
    }
}