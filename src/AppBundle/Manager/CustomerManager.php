<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Customer;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

class CustomerManager
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getRepository(): ObjectRepository
    {
        return $this->entityManager->getRepository(Customer::class);
    }

    public function save(Customer $customer)
    {
        $this->entityManager->persist($customer);
        $this->entityManager->flush();
    }
}
