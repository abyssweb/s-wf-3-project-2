<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Center;

class CardRepository extends \Doctrine\ORM\EntityRepository
{
    public function findCardCountFromCenter(Center $center): string
    {
        $result = $this->createQueryBuilder('card')
            ->select('count(card.id)')
            ->leftJoin('card.center', 'cc')
            ->where('cc.id = :center')
            ->setParameter('center', $center->getId())
            ->getQuery()
            ->getResult();

        return $result[0][1];
    }

    public function findLastTenCards(): array
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.id', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    public function findCardCount(): string
    {
        $result = $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->getQuery()
            ->getResult();
        return $result[0][1];
    }

    public function findLastTenActiveCards(): array
    {
        return $this->createQueryBuilder('c')
            ->select('DISTINCT c')
            ->leftJoin('c.gameDatas', 'g')
            ->where('c.disabledAt IS NULL')
            ->orderBy('g.date', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    public function findLastFiveActiveCards(): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.disabledAt IS NULL')
            ->orderBy('c.createdAt', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }

    public function findCardByPattern($query): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.cardPattern LIKE :query')
            ->setParameter(':query', '%' . $query . '%')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    public function findCardByHexId($query): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.cardHexId LIKE :query')
            ->setParameter(':query', '%' . $query . '%')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
}
