<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;

class OfferRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllAvailableOffersByUser(User $user): array
    {
        return $this->createQueryBuilder('o')
            ->select('o.name, o.shortDescription, o.description, o.startAt, o.endAt, uo.used, o.id')
            ->leftJoin('o.userOffers', 'uo')
            ->where('uo.user = :user')
            ->andWhere('uo.used = 0')
            ->andWhere('o.endAt > CURRENT_TIMESTAMP()')
            ->setParameter('user', $user->getId())
            ->getQuery()
            ->getResult();
    }

    public function findAllAvailableOffers()
    {
        return $this->createQueryBuilder('o')
            ->where('o.endAt > CURRENT_TIMESTAMP()')
            ->getQuery()
            ->getResult();
    }
}
