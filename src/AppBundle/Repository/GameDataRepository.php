<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\Query\Expr\Join;

class GameDataRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllGameDatasByUser(User $user): array
    {
        return $this->createQueryBuilder('g')
            ->select('g.score, g.victory, g.date')
            ->leftJoin('g.card', 'gc', Join::WITH, 'gc.user = :user')
            ->where('gc.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    public function findVictoryCount(User $user): string
    {
        $result = $this->createQueryBuilder('g')
            ->select('count(g.id)')
            ->leftJoin('g.card', 'gc', Join::WITH, 'gc.user = :user')
            ->where('gc.user = :user')
            ->andWhere('g.victory = 1')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
        return $result[0][1];
    }

    public function findDefeatCount(User $user): string
    {
        $result = $this->createQueryBuilder('g')
            ->select('count(g.id)')
            ->leftJoin('g.card', 'gc', Join::WITH, 'gc.user = :user')
            ->where('gc.user = :user')
            ->andWhere('g.victory = 0')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
        return $result[0][1];
    }

    public function findGameCount(User $user): string
    {
        $result = $this->createQueryBuilder('g')
            ->select('count(g.id)')
            ->leftJoin('g.card', 'gc', Join::WITH, 'gc.user = :user')
            ->where('gc.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
        return $result[0][1];
    }

    public function findHighScore(User $user): ?string
    {
        $result = $this->createQueryBuilder('g')
            ->select('max(g.score)')
            ->leftJoin('g.card', 'gc', Join::WITH, 'gc.user = :user')
            ->where('gc.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
        return $result[0][1];
    }

    public function findLowScore(User $user): ?string
    {
        $result = $this->createQueryBuilder('g')
            ->select('min(g.score)')
            ->leftJoin('g.card', 'gc', Join::WITH, 'gc.user = :user')
            ->where('gc.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
        return $result[0][1];
    }

    public function findAvgScore(User $user): ?string
    {
        $result = $this->createQueryBuilder('g')
            ->select('avg(g.score)')
            ->leftJoin('g.card', 'gc', Join::WITH, 'gc.user = :user')
            ->where('gc.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
        return $result[0][1];
    }
}
