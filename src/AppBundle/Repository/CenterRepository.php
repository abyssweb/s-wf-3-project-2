<?php

namespace AppBundle\Repository;

class CenterRepository extends \Doctrine\ORM\EntityRepository
{
    public function findLast5Centers(): array
    {
        return $this->createQueryBuilder('c')
            ->where('c.closedAt IS NULL')
            ->orderBy('c.id', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }
}
