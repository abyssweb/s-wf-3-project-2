<?php

namespace AppBundle\Repository;

class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function findUsersByEmailLike($query): array
    {
        return $this->createQueryBuilder('user')
            ->where('user.email LIKE :query')
            ->andWhere('user.role = \'ROLE_CUSTOMER\'')
            ->setParameter('query', '%' . $query . '%')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    public function findUserByCustomerName($query): array
    {
        return $this->createQueryBuilder('user')
            ->leftJoin('user.customer', 'customer')
            ->where('customer.lastname LIKE :query')
            ->andWhere('user.role = \'ROLE_CUSTOMER\'')
            ->setParameter('query', '%' . $query . '%')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    public function findUserByCardPattern($query): array
    {
        return $this->createQueryBuilder('user')
            ->leftJoin('user.cards', 'cards')
            ->where('cards.cardPattern LIKE :query')
            ->setParameter(':query', '%' . $query . '%')
            ->getQuery()
            ->getResult();
    }

    public function findUserByCardHexId($query): array
    {
        return $this->createQueryBuilder('user')
            ->leftJoin('user.cards', 'cards')
            ->where('cards.cardHexId LIKE :query')
            ->setParameter(':query', '%' . $query . '%')
            ->getQuery()
            ->getResult();
    }

    public function findLastTenUsers(): array
    {
        return $this->createQueryBuilder('u')
            ->where('u.role = \'ROLE_CUSTOMER\'')
            ->orderBy('u.id', 'DESC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    public function findLastFiveUsers(): array
    {
        return $this->createQueryBuilder('u')
            ->where('u.role = \'ROLE_CUSTOMER\'')
            ->orderBy('u.id', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }

    public function findUserCount(): string
    {
        $result = $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->where('u.role = \'ROLE_CUSTOMER\'')
            ->getQuery()
            ->getResult();
        return $result[0][1];
    }

    public function findLastFiveStaffMembers(): array
    {
        return $this->createQueryBuilder('u')
            ->where('u.role = \'ROLE_STAFF\'')
            ->orderBy('u.id', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }
}
