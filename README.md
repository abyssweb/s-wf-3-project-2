Projet 2 : Gestion de cartes de fidélité
========================================

##Sujet n°1

###1. Objectif
- Réaliser un projet de qualité, testé et documenté à présenter sur une soutenance.
- Proposer une couverture unitaire et fonctionnelle à la réalisation. 
- Créer un diagrammme UML an de visualiser les modèles de votre application.

###2. Contexte

"Shinigami Laser" est un laser game assez connu en Bourgogne. Malheureusement, le suivi des joueurs est aujourd'hui assez chaotique avec des fiches papier sur lesquelles sont écrits le nom, le surnom, l'adresse, le numéro de téléphone et la date de naissance d'un joueur. Ces fiches sont devenues totalement obsolètes, aussi nous aimerions nous doter d'un système de cartes de fidélité, cartes physiques ou numériques (avec une application sur smartphone comme interface).

Pour ce faire, nous avons contacté un fournisseur de cartes qui pourrait nous imprimer des cartes avec une puce NFC/RFID, un QR code optionnel, et bien sûr des informations précieuses sur notre club.

La carte elle-même aura un numéro. Celui-ci servira pour nos références internes, mais aussi à nos clients qui pourront les rattacher à leur compte en ligne pour suivre différentes informations : leurs visites, leurs scores, leurs offres, etc.

###3. Réalisation

- Proposer cette gestion de carte de fidélité, sachant que :
    - Une carte physique possèdera un motif : 
        - `CODE_CENTRE` `CODE_CARTE` `CHECKSUM`
            - `CODE_CENTRE` : 3 chiffres décrivant un établissement
            - `CODE_CARTE` : 6 chiffres décrivant un client 
            - `CHECKSUM` : somme des chiffres précédents modulo 9 
        - Exemple : 123 142121 8 
    - Réaliser une plate-forme web permettant les interactions suivantes : 
        - Un client pourra ouvrir son compte ; 
        - Un client pourra rattacher une carte de fidélité délivrée dans nos locaux ; 
        - Un client pourra pourra afficher des informations sur ses cartes de fidélité ; 
        - Un membre du staff pourra trouver un client d'après un numéro de carte.
        
Note :

Nous ne souhaitons pas dans l'immédiat gérer les informations relatives aux services associés à ces cartes (affichage des visites, scores ou autres) bien que cela soit évidemment un "plus" 

#### Notes personnelles :

Le fournisseur fournit des cartes physiques imprimées en couleur, avec une case vide pour imprimer le motif lors de la délivrance de la carte.
Le Staff peut délivrer une carte dans les locaux, en enregistrant la carte et en y liant les parties et informations de la session de jeu, il génère un motif, et celui-ci est imprimé sur la carte dans la case vide, et la donne au client

Un Client peut ouvrir un compte en ligne.
Un Client peut rattacher une carte à son compte.
Un Client pourra dans une version future du site, réserver et payer ses parties en ligne.
Un Client peut rattacher autant de cartes qu'il veut à son compte (en cas de perte ou de vol). Seule la dernière carte enregistrée est active. Les autres sont considérées comme désactivées. Une carte possède un motif unique, un motif ne peut pas être attribué à plusieurs cartes.

Un Compte peut avoir plusieurs cartes rattachées, mais seulement une sera active, la dernière enregistrée.

La carte est un raccourci pour le Staff afin d'identifier ses clients. Les informations relatives au client sont stockées en base de données et liées à son compte. Le motif de la carte sert d'identifiant pour le Client lors de son passage au centre.

Le Staff peut retrouver les informations d'un Client depuis le motif de sa carte.
Dans une version future, le Staff pourra créer son Client en ligne.

* Choix :
    - PHP : 7.1.11
    - DB : SQLite
    - Symfony : 3.3.13
    
#### TODO :

- Formulaires : Affichage des erreurs à revoir

- Préparation Soutenance :
    - Diaporama de présentation,
    - Ecriture des scénarios,
    - Création et vérification de profils et cartes de tests.
    
* Utilisateurs :
    - 3 rôles : Admin, Staff, Customer,
    - Admin : Users, Centers,
    - Staff : Cards, Offers, Games
    - Customer : Account, Profile
    
* Centers :
    - Creation, update, display, closing,
* Cards :
    - Staff : Creation, display, link to Customer,
    - Customer : Link to self, display datas,
* Offers :
    - Staff : Creation, update, display, givaway, use
    - Customer : Add to self,